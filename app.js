const APIURL =
  "https://api.themoviedb.org/3/discover/movie?sort_by=popularity.desc&api_key=04c35731a5ee918f014970082a0088b1&page=1";
const IMGPATH = "https://image.tmdb.org/t/p/w1280";
const SEARCHAPI =
  "https://api.themoviedb.org/3/search/movie?&api_key=04c35731a5ee918f014970082a0088b1&query=";

const main = document.getElementById("main");
const form = document.getElementById("form");
const search = document.getElementById("search");
const chosenMovie = document.getElementById("chosen-movie");

getMovies(APIURL);

async function getMovies(url) {
  const response = await fetch(url);
  const data = await response.json();
  setMovies(data.results);
  console.log(data.results);
  return data;
}
function setMovies(movieArray) {
  main.innerHTML = "";
  document.body.classList.remove("background-class");
  movieArray.forEach((movies, index) => {
    const movieContainer = document.createElement("div");
    movieContainer.classList.add("movie");
    movieContainer.setAttribute("id", index);
    movieContainer.innerHTML = `
        <img src="${IMGPATH + movies.poster_path}" alt="${movies.title}">
        <div class="movie-info">
            <h3 class="movie-title">${movies.title}</h3>
            <span class="${classVote(movies.vote_average)}">${
      movies.vote_average
    }</span>
        </div>
        <div class="overview">
        <h3>Overview:</h3>
        ${movies.overview}
    </div>
     `;
    main.appendChild(movieContainer);
   
    movieContainer.addEventListener("click", () => {
      main.innerHTML = ''
      document.body.classList.add("background-class")
      document.body.style.backgroundImage = `url(${IMGPATH + movies.backdrop_path})`
      chosenMovie.innerHTML =`
      <div class="movie-header">
           <img src="${IMGPATH + movies.poster_path}" alt="${movies.title}">
           <a href="index.html" class="back-to-link">back to home page</a>
      </div>
        <div class="movie-detail">
           <h3>${movies.title}</h3>
           <h4>${movies.overview}</h4>
        </div>
      ` 
    });
  });
}
function classVote(vote) {
  if (vote >= 8) {
    return "green";
  } else if (vote >= 5) {
    return "orange";
  } else {
    return "red";
  }
}

form.addEventListener("submit", (e) => {
  e.preventDefault();
  const searchValue = search.value;
  if (searchValue) {
    getMovies(SEARCHAPI + searchValue);
    search.value = "";
  }
});
